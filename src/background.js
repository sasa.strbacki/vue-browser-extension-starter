async function getHistory() {
  const startDate = new Date();
  startDate.setMonth(startDate.getMonth() - 1);

  let history = await chrome.history.search({ text: '', maxResults: 0, startTime: startDate.getTime() });

  history = history.map(historyItem => {

    const { hostname } = new URL(historyItem.url)

    return {
      ...historyItem,
      url: hostname
    }
  });

  return history;
}

async function getExtensions(){
  let extensions = await chrome.management.getAll();
  return extensions
}

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  switch (request.type) {
    case "GET_HISTORY":
      getHistory().then(sendResponse);
      return true;

    case "GET_EXTENSIONS":
      getExtensions().then(sendResponse);
      return true;

    default:
      break;
  }
});

chrome.management.onInstalled.addListener((extensionInfo) => {
    console.log(extensionInfo);
  }
);